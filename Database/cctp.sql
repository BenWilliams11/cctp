-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2021 at 02:35 AM
-- Server version: 8.0.22
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cctp`
--

-- --------------------------------------------------------

--
-- Table structure for table `enemycharacters_referance`
--

CREATE TABLE `enemycharacters_referance` (
  `Enemy` text NOT NULL,
  `EnemyID` int NOT NULL,
  `Health` smallint NOT NULL,
  `Attack` smallint NOT NULL,
  `Defence` smallint NOT NULL,
  `Speed` tinyint NOT NULL,
  `Attack_Range` tinyint NOT NULL,
  `Resistance` tinyint NOT NULL,
  `EXP when hit` smallint NOT NULL,
  `EXP when killed` smallint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `enemycharacters_referance`
--

INSERT INTO `enemycharacters_referance` (`Enemy`, `EnemyID`, `Health`, `Attack`, `Defence`, `Speed`, `Attack_Range`, `Resistance`, `EXP when hit`, `EXP when killed`) VALUES
('Abomination', 5, 130, 35, 5, 3, 3, 4, 5, 20),
('Novice Necromancer', 4, 70, 15, 4, 3, 3, 3, 4, 15),
('Skeleton Archer', 2, 20, 5, 2, 2, 2, 2, 1, 4),
('Undead Brute', 3, 60, 10, 5, 1, 1, 3, 3, 10),
('Undead Warrior', 1, 30, 5, 2, 2, 1, 2, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `Item` text NOT NULL,
  `Effect` text NOT NULL,
  `Quantity` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`Item`, `Effect`, `Quantity`) VALUES
('Bomb', 'Deals -20 damage to enemy\'s. Range: 2', 5),
('Health Potion', 'Heals +30 HP', 5),
('Strength Potion ', 'Adds +5 to the characters Attack for the next turn.', 3);

-- --------------------------------------------------------

--
-- Table structure for table `playercharacters_referance`
--

CREATE TABLE `playercharacters_referance` (
  `ID` varchar(5) NOT NULL,
  `Class` varchar(30) NOT NULL,
  `Level` smallint NOT NULL,
  `EXP_Gained` int NOT NULL,
  `Health` smallint NOT NULL,
  `Attack` smallint NOT NULL,
  `Defence` smallint NOT NULL,
  `Speed` tinyint NOT NULL,
  `Attack_Range` tinyint NOT NULL,
  `Resistance` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `playercharacters_referance`
--

INSERT INTO `playercharacters_referance` (`ID`, `Class`, `Level`, `EXP_Gained`, `Health`, `Attack`, `Defence`, `Speed`, `Attack_Range`, `Resistance`) VALUES
('1', 'Fighter', 1, 0, 90, 10, 3, 2, 1, 2),
('2', 'Hunter', 1, 0, 80, 10, 3, 2, 2, 2),
('3', 'Recruit', 1, 0, 100, 4, 4, 1, 1, 3),
('4', 'Apprentice', 1, 0, 70, 10, 1, 2, 2, 2),
('5', 'Thief', 1, 0, 80, 7, 2, 3, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `playercharacters_update`
--

CREATE TABLE `playercharacters_update` (
  `ID` int NOT NULL,
  `UserID` int NOT NULL,
  `usersUid` varchar(128) NOT NULL,
  `CharacterID` int NOT NULL,
  `Name` varchar(130) NOT NULL,
  `Class` varchar(50) NOT NULL,
  `Level` smallint NOT NULL,
  `EXP_Gained` int NOT NULL,
  `Health` int NOT NULL,
  `Attack` int NOT NULL,
  `Defence` int NOT NULL,
  `Speed` int NOT NULL,
  `Attack_Range` int NOT NULL,
  `Resistance` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `playercharacters_update`
--

INSERT INTO `playercharacters_update` (`ID`, `UserID`, `usersUid`, `CharacterID`, `Name`, `Class`, `Level`, `EXP_Gained`, `Health`, `Attack`, `Defence`, `Speed`, `Attack_Range`, `Resistance`) VALUES
(1, 1, 'BenW', 1, 'Ben', 'Fighter', 1, 0, 90, 10, 3, 2, 1, 2),
(2, 2, 'test', 2, 'Bob', 'Hunter', 1, 0, 80, 10, 3, 2, 2, 2),
(3, 3, 'test1', 3, 'Doug', 'Recruit', 1, 0, 100, 4, 4, 1, 1, 3),
(4, 4, 'test2', 4, 'Doug2Electirc Boogaloo', 'Apprentice', 1, 0, 70, 10, 1, 2, 2, 2),
(13, 3, 'test1', 1, 'Doom Guy', 'Fighter', 1, 0, 90, 10, 3, 2, 1, 2),
(31, 9, 'Jon', 1, 'Jon', 'Fighter', 1, 0, 90, 10, 3, 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usersId` int NOT NULL,
  `usersName` varchar(128) NOT NULL,
  `usersEmail` varchar(128) NOT NULL,
  `usersUid` varchar(128) NOT NULL,
  `usersPwd` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usersId`, `usersName`, `usersEmail`, `usersUid`, `usersPwd`) VALUES
(1, 'Ben Williams', 'benw664@gmail.com', 'BenW', '$2y$10$P702S6aGzGfQuYWdq0ywNe3Oean/336r7f.0uMmToi77VHTLHXuda'),
(2, 'Test', 'testemail@test.com', 'test', '$2y$10$UByrvge6QAnLXplJoPFEEe/OTK./9Grvc.Rhz39d7XP8rAd6kx/0.'),
(3, 'test1', 'test1email@test.com', 'test1', '$2y$10$8AKzbCyqmepot8z0Q6dR2efnaWNDKpQwMXwctwSzkEB.BLfcCdjc6'),
(4, 'test2', 'test2email@test.com', 'test2', '$2y$10$aRvEWF2G4gorA.ObUR3dgu3LgCCmgg3wbW7vSI3iH5f6hHFjJJ7U2'),
(8, 'test0', 'test0email@test.com', 'test0', '$2y$10$X.6OC64YW./XfTI0FVrq1uJ.Wbyw3XwVPl3bApPSaIwMaq/fSTX/S'),
(9, 'Jon', 'jonemail@test.com', 'Jon', '$2y$10$uRYw9EKOEu938iebvnCvwOgeFxeFi1dKUIhBakl/kEGr7i2xmXsu2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enemycharacters_referance`
--
ALTER TABLE `enemycharacters_referance`
  ADD PRIMARY KEY (`Enemy`(30));

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`Item`(30));

--
-- Indexes for table `playercharacters_referance`
--
ALTER TABLE `playercharacters_referance`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `playercharacters_update`
--
ALTER TABLE `playercharacters_update`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usersId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playercharacters_update`
--
ALTER TABLE `playercharacters_update`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usersId` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
