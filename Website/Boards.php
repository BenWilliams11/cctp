<?php
include_once 'includes/header.php';
require_once 'includes/DisplayCharacterStats.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Leaderboards</title>
</head>

<body>
    <section class="section1">
        <article class="overflowbox">
            <h1>Leaderboards</h1>
            <!--Includes the sort table script in order to call the functions that sort the table, currently there is no numerical search-->
             <script src="includes/SortTable.js"></script>
                <table id="table">
                    <tr>
                        <th onclick="sortTable(0)">Name</th>
                        <th onclick="sortTable(1)">Class</th>
                        <th>Attack Power</th>
                        <th>Level</th>
                        <th>Experiance Points</th>
                    </tr>
                    <?php
                        //Display Users characters and stats
                        DisplayUsersCharacters($conn);
                    ?>
                </table>
        </article>

        <article class="leaderboards2">
            <img src="images/dragon-minis.png" width="350" height="200">
        </article>
    </section>

 <?php
 include_once 'includes/footer.php';
 ?>
</body>
</html>