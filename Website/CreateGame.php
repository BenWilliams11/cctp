<?php
include_once 'includes/header.php';
require_once 'includes/ErrorHandling.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create Game</title>
</head>

<body>
    <section class="section1">
        <article class="overflowbox"> 
         <h1>Create Game</h1>      
             <!--Form that takes each player name and character name, checks to see if they exist and puts all of the varaibles into the game file for the game to function-->
             <form action="includes/CreateGameInclude.php" method="post">
             <label for="player1name"><b>Player 1 Username:</b></label>
             <input type="text" name="player1name" placeholder="Player 1 Username"/>
             <br />
             <br />
             <label for="player1character"><b>Player 1 Character Name:</b></label>
             <input type="text" name="player1character" placeholder="Player 1 Character Name"/>
             <br />
             <br />
             <label for="player2name"><b>Player 2 Username:</b></label>
             <input type="text" name="player2name" placeholder="Player 2 Username"/>
             <br />
             <br />
             <label for="player2character"><b>Player 2 Character Name:</b></label>
             <input type="text" name="player2character" placeholder="Player 2 Character Name"/>
             <br />
             <br />
             <label for="player3name"><b>Player 3 Username:</b></label>
             <input type="text" name="player3name" placeholder="Player 3 Username"/>
             <br />
             <br />
             <label for="player3character"><b>Player 3 Character Name:</b></label>
             <input type="text" name="player3character" placeholder="Player 3 Character Name"/>
             <br />
             <br />
             <label for="player4name"><b>Player 4 Username:</b></label>
             <input type="text" name="player4name" placeholder="Player 4 Username"/>
             <br />
             <br />
             <label for="player4character"><b>Player 4 Character Name:</b></label>
             <input type="text" name="player4character" placeholder="Player 4 Character Name"/>
             <br />
             <br />
             <button type="submit" name="submit">Create Game</button>
             </form>
             <br />
             <br />
              <?php 
              //error handling
              CreateGameErrorHandling();
              ?>
        </article>
    </section>

<?php
include_once 'includes/footer.php';
?>
</body>
</html>