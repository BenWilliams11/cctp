<?php
include_once 'includes/header.php';
require_once 'includes/DisplayCharacterStats.php';
require_once 'includes/SessionVariables.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Game</title>
</head>

<body>
   <section class="section1">
        <article class="overflowbox">
            <?php        
              echo "<b>Game Master: ".$userUID. "</b>"; 
            ?>
            <br />
            <img src="images/profilepic1.jpg" alt="Placeholder image" width="100" height="100" />
            <!--Image not royaly free. Taken from: https://www.google.com/search?q=d%26d+character+portraits&sxsrf=ALeKk02kDN3b50nYSraBDAiy3-A503eAgA:1619639752387&source=lnms&tbm=isch&sa=X&ved=2ahUKEwigiOug3KHwAhViqnEKHZTzBEMQ_AUoAXoECAEQAw&biw=1377&bih=659#imgrc=ubWD-xjOSFxr3M -->
            <br />
            <table>
                <tr>
                    <th>Name</th>
                    <th>Class</th>
                    <th>Level</th>
                    <th>EXP Gained</th>
                    <th>HP</th>
                    <th>Atk</th>
                    <th>Def</th>
                    <th>Spd</th>
                    <th>Rng</th>
                    <th>Res</th>
                </tr>

            <?php

            echo "<b>".$player1name. "</b>";

            // Displays character infomration, passes in both player and character name to display the correct info
            CharacterUpdate($conn, $player1name, $player1charname);
            ?>

            </table>
            <br />
            <br />
            <form action="includes/UpdateStatsPlayer1.php" method="post">
                <input type="number" name="level" placeholder="Level" />
                <input type="number" name="exp" placeholder="EXP Gained" />
                <br />
                <input type="number" name="health" placeholder="Health" />
                <input type="number" name="attack" placeholder="Attack" />
                <br />
                <input type="number" name="defence" placeholder="Defence" />
                <input type="number" name="speed" placeholder="Speed" />
                <br />
                <input type="number" name="attack_range" placeholder="Attack_Range" />
                <input type="number" name="resistance" placeholder="Resistance" />
                <br />
                <button type="submit" name="submit">Update</button>
            </form>
            <br />
          
              <br />
              <img src="images/profilepic2.jpg" alt="Placeholder image" width="100" height="100" />
            <!--Image not royaly free. Taken from: https://www.google.com/search?q=d%26d+character+portraits&sxsrf=ALeKk02kDN3b50nYSraBDAiy3-A503eAgA:1619639752387&source=lnms&tbm=isch&sa=X&ved=2ahUKEwigiOug3KHwAhViqnEKHZTzBEMQ_AUoAXoECAEQAw&biw=1377&bih=659#imgrc=r_9XsVmz42lDlM -->
             <br />
            <table>
                <tr>
                    <th>Name</th>
                    <th>Class</th>
                    <th>Level</th>
                    <th>EXP Gained</th>
                    <th>HP</th>
                    <th>Atk</th>
                    <th>Def</th>
                    <th>Spd</th>
                    <th>Rng</th>
                    <th>Res</th>
                </tr>
            <?php
            echo "<b>".$player2name. "</b>";
            // Displays character infomration, passes in both player and character name to display the correct info
            CharacterUpdate($conn, $player2name, $player2charname);
            ?>

                </table>
                <br />
                <br />
                <form action="includes/UpdateStatsPlayer2.php" method="post">
                    <input type="number" name="level" placeholder="Level" />
                    <input type="number" name="exp" placeholder="EXP Gained" />
                    <br />
                    <input type="number" name="health" placeholder="Health" />
                    <input type="number" name="attack" placeholder="Attack" />
                    <br />
                    <input type="number" name="defence" placeholder="Defence" />
                    <input type="number" name="speed" placeholder="Speed" />
                    <br />
                    <input type="number" name="attack_range" placeholder="Attack_Range" />
                    <input type="number" name="resistance" placeholder="Resistance" />
                    <br />
                    <button type="submit" name="submit">Update</button>
                </form>
            <br/>
             <img src="images/profilepic3.jpg" alt="Placeholder image" width="100" height="100" />
            <!--Image not royaly free. Taken from: https://www.google.com/search?q=d%26d+character+portraits&sxsrf=ALeKk02kDN3b50nYSraBDAiy3-A503eAgA:1619639752387&source=lnms&tbm=isch&sa=X&ved=2ahUKEwigiOug3KHwAhViqnEKHZTzBEMQ_AUoAXoECAEQAw&biw=1377&bih=659#imgrc=XUkIbm9MNOAPiM -->
            <br/>
                <?php
                echo "<b>".$player3name. "</b>";
                ?>

                <table>
                    <tr>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>

                <?php

                // Displays character infomration, passes in both player and character name to display the correct info
                CharacterUpdate($conn, $player3name, $player3charname);
                ?>

                </table>
                <br />
                <br />
                <form action="includes/UpdateStatsPlayer3.php" method="post">
                    <input type="number" name="level" placeholder="Level" />
                    <input type="number" name="exp" placeholder="EXP Gained" />
                    <br />
                    <input type="number" name="health" placeholder="Health" />
                    <input type="number" name="attack" placeholder="Attack" />
                    <br />
                    <input type="number" name="defence" placeholder="Defence" />
                    <input type="number" name="speed" placeholder="Speed" />
                    <br />
                    <input type="number" name="attack_range" placeholder="Attack_Range" />
                    <input type="number" name="resistance" placeholder="Resistance" />
                    <br />
                    <button type="submit" name="submit">Update</button>
                </form>
                <img src="images/profilepic4.jpg" alt="Placeholder image" width="100" height="100" />
                <!--Image not royaly free. Taken from: https://www.google.com/search?q=d%26d+character+portraits&sxsrf=ALeKk02kDN3b50nYSraBDAiy3-A503eAgA:1619639752387&source=lnms&tbm=isch&sa=X&ved=2ahUKEwigiOug3KHwAhViqnEKHZTzBEMQ_AUoAXoECAEQAw&biw=1377&bih=659#imgrc=WMHoOb83_0XxiM -->
                <br />
                <?php
                echo "<b>".$player4name."</b>";
                ?>

                <table>
                    <tr>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                <?php

                // Displays character infomration, passes in both player and character name to display the correct info
                CharacterUpdate($conn, $player4name, $player4charname);
                ?>

                </table>
                <br />
                <br />
                <form action="includes/UpdateStatsPlayer4.php" method="post">
                    <input type="number" name="level" placeholder="Level" />
                    <input type="number" name="exp" placeholder="EXP Gained" />
                    <br />
                    <input type="number" name="health" placeholder="Health" />
                    <input type="number" name="attack" placeholder="Attack" />
                    <br />
                    <input type="number" name="defence" placeholder="Defence" />
                    <input type="number" name="speed" placeholder="Speed" />
                    <br />
                    <input type="number" name="attack_range" placeholder="Attack_Range" />
                    <input type="number" name="resistance" placeholder="Resistance" />
                    <br />
                    <button type="submit" name="submit">Update</button>
                </form>
                <br />
                <br />
                <br />
                <br />

                <!--Enemys are harcoded at the moment-->
                <h1>Enemy' s</h1>
                <img src="images/enemy.jpg" alt="Placeholder image" width="100" height="100" />
                 <!--Image not royaly free. Taken from: https://www.google.com/search?q=skeleton+warrior+portrait&client=opera-gx&hs=lEv&sxsrf=ALeKk00Z5jtHOCWFwcKcgvkT36xWHhWH8w:1619644790429&source=lnms&tbm=isch&sa=X&ved=2ahUKEwid55SD76HwAhWEThUIHSQWD6IQ_AUoAXoECAEQAw&biw=1377&bih=659#imgrc=l6t6fJLXKwNVAM -->
          <table>
              <tr>
                  <th>Enemy</th>
                  <th>Health</th>
                  <th>Attack</th>
                  <th>Defence</th>
                  <th>Speed</th>
                  <th>Attack_Range</th>
                  <th>Resistance</th>
                  <th>EXP when hit</th>
                  <th>EXP when killed</th>
              </tr>
          <?php
            $enemyID = "1";
            DisplayEnemyStatistics($conn, $enemyID);
          ?>

          </table>
          <br />
          <br />
          <form action="includes/UpdateEnemyStats.php" method="post">
              <input type="number" name="health" placeholder="Health" />
              <button type="submit" name="submit">Update</button>
          </form>


         <img src="images/enemy2.jpg" alt="Placeholder image" width="100" height="100" />
         <!--Image not royaly free. Taken from: https://www.google.com/search?q=skeleton+archer+portrait&client=opera-gx&hs=pma&sxsrf=ALeKk03pul2GlIsNFK42U_qR-APm3DlWyg:1619645619291&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiiwrKO8qHwAhU0RhUIHdXdDI0Q_AUoAXoECAEQAw&biw=1377&bih=659#imgrc=cpNDfvQFVv-SFM -->
          <table>
              <tr>
                  <th>Enemy</th>
                  <th>Health</th>
                  <th>Attack</th>
                  <th>Defence</th>
                  <th>Speed</th>
                  <th>Attack_Range</th>
                  <th>Resistance</th>
                  <th>EXP when hit</th>
                  <th>EXP when killed</th>
              </tr>
          <?php
            $enemyID = "2";
            DisplayEnemyStatistics($conn, $enemyID);
          ?>

          </table>
          <br />
          <br />
          <form action="includes/UpdateEnemyStats2.php" method="post">
              <input type="number" name="health" placeholder="Health" />
              <button type="submit" name="submit">Update</button>
          </form>

        </article>

    </section>

<?php
include_once 'includes/footer.php';
?>
</body>
</html>