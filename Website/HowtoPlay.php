<?php
include_once 'includes/header.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        function openTab(th) {
            window.open(th.name, '_blank');
        }
    </script>

    <title>How To Play</title>
</head>
<body>
     <section class="section1">
        <article class="overflowbox">
            <!--The rule book in html form-->
            <h1>Rules</h1>

                <h3>Introduction</h3>

                <a>
                    In this turn-based RPG players can take control of many different adventurers aswell as becoming the 
                    game master who guides the adventurers through the game aswell as take control of the enemies. 
                    Players upon buying this game will receive 5 models in which they can scan into the website in 
                    order to take control of that character aswell as one pre-set campaign to play. Players can 
                    create and load different campaigns with this website aswell as track leader boards and purchase 
                    more characters.
                    In this campaign, The Mountains of Madness there are 5 characters that the players can pick between 
                    aswell as the game master role. Once these roles have been chosen the game master will set up the 
                    game and each player will scan their model in which will show the game master the characters stats 
                    and it will allow them to change the players stats as the campaign is ongoing. Info on setting 
                    everything up will be detailed in Getting Started.
                    The game master upon setting up the game will have to look at the campaign book which will detail 
                    the basics of the story allowing for the game master to fill in more details if they want to, the 
                    book will also detail the level layouts and unit placements for players and enemies.
                </a>

                <h3>Game Info</h3>
                <a>This game is a turn-based RPG in which the players take turns to move and attack their enemies while 
                    the game master takes turns moving and attacking with the enemies. The game board consists of an 8x8 
                    square tile grid that the players and enemies can move on.</a>

                <h3>No of players, time it takes to play and ages-</h3>

                <a>Number of Players: 6, Time to play: 30 mins - 1 hour, Ages: 15+</a>

                <h3>Victory-</h3>

                <a>In this campaign there will be three levels that the player units will have to get through. If they 
                    all die during a level then the game is over, if they survive the campaign then they win and save 
                    the village.</a>

                <h3>Game Components</h3>
                <a>
                    This game upon initial purchase will have the components for the pre-set campaign: The Mountains of 
                    Madness. Expansions and other campaigns can be purchased separately.</br>
                    -	x16 Mountain Tiles, x8 Dirt Tiles, x12 Tree Tiles, x4 Dirt Corner Tiles,x29 Grass Tiles,
                    x35 castle floor tiles, x29 Cave Wall Tiles, x6 Cave Insidetiles.</br>
                    -	x5 Player Models, x2 Undead Models, x2 Skeleton Archer Models, x2 Undead Brute Models, x1 Novice 
                    Necromancer Model, x1 Abomination Model.</br>
                    -	x1 Rule Book</br>
                    -	x1 Campaign Book</br>
                    -	A Set of Dice
                </a>

                <h3>Getting Started</h3>
                    <a>
                    First  get together a party of 5, 4 players  and 1 Game  Master. The game master then goes onto the website
                    and sets up the game. Beforehand, the players would get the character that they want and scan them into the
                    website and add that character to their profile for later use. The Game Master then inputs  the  usernames
                    and  character  names  that  the  users  want  and  creates  the  game.  The  Game Master will then be able
                    to relay information to the players on the statistics of their characters aswell as update them asthe game goes on.
                    The players can also go to their profile and view the information of their characters as the game is ongoing.
                    </a>

                <h3>Game Mechanics</h3>

                <a>In this game each unit can perform 2 actions on their turn for example moving and attacking or moving 
                    and defending or attacking and attacking. This goes for enemies as well.</a>

                <h3>Moving-</h3>
                <a>On the board units can move in 4 directions: up, down, left, and right. Units can move the number of 
                    spaces according to their speed stat for example a unit with 2 speed can move two spaces on the 
                    board.</a>
                <h3>Attacking-</h3>
                <a>If a unit wants to attack another unit then they first need to determine if they are in range with 
                    their range stat, if the enemy unit is within the number of spaces that your range stat states then 
                    you can attack. When a unit attacks the game, master takes the units attack stat and minuses the 
                    enemy’s defence to get the damage done. The game master then rolls a D10 for the defender to see 
                    if they resisted the hit, units have a resistance stat of 1-10 and if the game master rolls the 
                    number of your resistance or lower than the defending unit successfully resists which increase 
                    their defence.</a>
                <h3>Defend-</h3>
                <a>If a unit choses to defend then instead of attacking they boost their defence to take any hits from 
                    the next turn. This will absorb around 60-70% of incoming damage.</a>
                <h3>Items-</h3>
                <a>A unit can choose to use an item that they have on them in this campaign all units will have a set 
                    number of items and no more. Different items will have different effects like a healing potion or 
                    mana potion.</a>
                <h3>Leveling Up-</h3>
                <a>Levelling up is not as prevalent in this smaller campaign but there will be a couple of level ups 
                    throughout this campaign. When a character levels up their stats will go up a set amount. Characters 
                    get exp from the monsters that they kill a set amount if they did damage to the enemy and an amount 
                    to the character who defeats the enemy.</a>
                <h3>Stats:</h3>
                <a>
                    <b>Heath-</b> A units HP points if they hit zero then that unit is down for the encounter. If all 
                    units are down then the game ends. If the encounter is cleared then the units wake back up with a 
                    quarter health.<br />
                    <b>Attack-</b> A unit’s attack points determines how strong their attack will be.</br>
                    <b>Defence-</b> A unit’s defence points determines how much damage they will block.</br>
                    <b>Speed-</b> A unit’s speed determines how far they can travel during each turn on the board.</br>
                    <b>Range-</b> A unit’s range determines how many spaces away the enemy has to be before the unit can 
                    attack, for example if a unit has a range of 3 then they can attack an enemy 3 spaces away.</br>
                    <b>Resistance-</b> A unit’s resistance determines how likely they are to resist an attack. If they 
                    resist then their damage is reduced greatly.
                </a>
                <h3>Characters</h3>
                <h3>Players:</h3>
                <a>
                    These 5 characters all come from the same village in the campaign. After a the undead came to attack 
                    the village and its people these 5 banded together in order to travel up the mountain in order to 
                    investigate the source of the undead.</br>
                    <b>The Fighter-</b> Eager to become a solider and one day join someone’s army, they go through 
                    rigours training each day. Wields a sword, is fast and strong but does not have much defence.</br>
                    <b>The Hunter-</b> Child of the villages Chief hunter who taught them everything that they know. 
                    Wields a bow, has good range and average stats.</br>
                    <b>The Recruit-</b> A recruit in training to become a knight who protects and acts as law enforcement 
                    in the village. Wields spear, good defence but slow and low attack power.</br>
                    <b>The Apprentice-</b> An apprentice of the village’s mage who tends to the villages needs while 
                    their master is away. Wields basic magic spells, good range, and attack but very low defence.</br>
                    <b>The Thief-</b> A thief who grew up stealing from the more fortunate in order to survive and help 
                    others. Wields dagger, fast with high resistance but low defence.
                </a>
                <h3>Level 1:</h3>
                <table class="how2_char_tab">
                    <tr>
                        <th>Class</th>
                        <th>Health</th>
                        <th>Attack</th>
                        <th>Defence</th>
                        <th>Speed</th>
                        <th>Range</th>
                        <th>Resistance</th>
                    </tr>
                    <tr>
                        <td>Fighter</td>
                        <td>90</td>
                        <td>10</td>
                        <td>3</td>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>Hunter</td>
                        <td>80</td>
                        <td>6</td>
                        <td>3</td>
                        <td>2</td>
                        <td>2</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>Recruit</td>
                        <td>100</td>
                        <td>5</td>
                        <td>4</td>
                        <td>1</td>
                        <td>1</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>Apprenctice</td>
                        <td>70</td>
                        <td>10</td>
                        <td>1</td>
                        <td>2</td>
                        <td>2</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>Thief</td>
                        <td>80</td>
                        <td>7</td>
                        <td>2</td>
                        <td>3</td>
                        <td>1</td>
                        <td>4</td>
                    </tr>
                </table>

                <h3>Level Up</h3>
                    <table class="how2_char_tab">
                    <tr>
                        <th>Level</th>
                        <th>EXP Needed</th>
                        <th>Health</th>
                        <th>Attack</th>
                        <th>Defence</th>
                        <th>Resistance</th>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>0</td>
                        <td>base</td>
                        <td>base</td>
                        <td>base</td>
                        <td>base</td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>10</td>
                        <td>+5</td>
                        <td>+5</td>
                        <td>+2</td>
                        <td>0</td>
                    </tr>

                    <tr>
                        <td>3</td>
                        <td>20</td>
                        <td>+10</td>
                        <td>+3</td>
                        <td>+2</td>
                        <td>+1</td>
                    </tr>
                    </table>

                <h3>Enemy’s:</h3>
                <a>
                    <b>Undead Warrior-</b> Weak fodder enemy, slow and low defence but strong together in packs, try keeping 
                    them apart.</br>
                    <b>Skeleton Archer-</b> Weak fodder enemy, very low defence but they do have range, try rushing them or 
                    attacking with another ranged unit.</br>
                    <b>Undead Brute-</b> strong with high hp and average defence but slow, try attacking with multiple 
                    units or with ranged units.</br>
                    <b>Novice Necromancer-</b>strong with high hp and range, but low defence try defending his attacks 
                    while other units attack and whittle him down. Every turn the Necromancer rolls a D10 if that lands 
                    on 10 then the Abomination spawns.</br>
                    <b>Abomination-</b> Potential summon from necromancer by sacrificing himself. Spawns very strong 
                    creature with high stats. You are going to need all the party together to take this one down.
                </a>

                <table>
                    <tr>
                        <th>Enemy</th>
                        <th>Health</th>
                        <th>Attack</th>
                        <th>Defence</th>
                        <th>Speed</th>
                        <th>Range</th>
                        <th>Resistance</th>
                        <th>EXP When Hit</th>
                        <th>EXP when defeated</th>
                    </tr>
                    <tr>
                        <td>Undead Warrior</td>
                        <td>30</td>
                        <td>5</td>
                        <td>2</td>
                        <td>2</td>
                        <td>1</td>
                        <td>2</td>
                        <td>1</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Skeleton Archer</td>
                        <td>20</td>
                        <td>5</td>
                        <td>2</td>
                        <td>2</td>
                        <td>2</td>
                        <td>2</td>
                        <td>1</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td>Undead Brute</td>
                        <td>60</td>
                        <td>10</td>
                        <td>5</td>
                        <td>1</td>
                        <td>1</td>
                        <td>3</td>
                        <td>3</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>Novice Necromancer</td>
                        <td>70</td>
                        <td>15</td>
                        <td>4</td>
                        <td>3</td>
                        <td>3</td>
                        <td>3</td>
                        <td>4</td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>Abomination</td>
                        <td>130</td>
                        <td>35</td>
                        <td>5</td>
                        <td>3</td>
                        <td>3</td>
                        <td>4</td>
                        <td>5</td>
                        <td>20</td>
                    </tr>
                </table>
            <br />
            <br />
            <br />
            <br />

        </article>

        <article class="downloadrules">
  
            <img id="dd-logo"src="images/dragon-minis.png" alt="Placeholder image" width="350" height="200">
            <br />
            <br />
           <button onClick="openTab(this)" href="#" name="external_documents/rule_book.pdf">Download Rule Book</button>
            <br />
            <br />
           <button onClick="openTab(this)" href="#" name="external_documents/campaign_book.pdf">Download Campaign Book</button>
        </article>

</section>
      
<footer>
     <?php include_once 'includes/footer.php';?>
</footer> 

</body>
</html>