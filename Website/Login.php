<?php
include_once 'includes/header.php';
require_once 'includes/ErrorHandling.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
</head>

<body>
   <section class="section1">
        <article class="overflowbox"> 
            <h1>Login</h1>

                 <form action="includes/LoginInclude.php" method="post">
                    <input type="text" name="uid" placeholder="Username/Email..." />
                     <br />
                     <br />
                    <input type="password" name="pwd" placeholder="Password..." />
                    <br />
                     <br />
                    <button type="submit" name="submit">Log In</button>
                </form>

                 <?php 
                 //Error Handling
                 LoginErrorHandling()
                 ?>
        </article>

    </section>

<?php
include_once 'includes/footer.php';
?>
</body>
</html>