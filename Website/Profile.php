 <?php
 include_once 'includes/header.php';
 require_once 'includes/SessionVariables.php';
 require_once 'includes/DisplayCharacterStats.php';
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Profile</title>
</head>

<body>
   <section class="section1">
        <article class="overflowbox"> 
            <?php
            echo "<h1> Username: " .$_SESSION["useruid"] . "</h1>";
            ?>
                <h3>Current Characters</h3>
                 <table>
                    <tr>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                    <?php
                    //Display all characters that the user has
                    ProfileCharacterStats($conn, $userID)
                    ?>
                </table>

             <form action='includes/DeleteCharacterInclude.php' method='post'>
              <label for="charname">Input Character Name to Delete Character:</label><br>
             <input type="text" name="charname" placeholder="Character Name"/>
             <button type='submit' name='submit'>Delete Character</button>
             </form>
             <br />
             <br />
             <br />
             <br />
             <br />
             <form action="includes/DeleteAccountInclude.php" method="post">
             <button type="submit" name="submit">Delete Profile</button>
             </form>
        </article>
    </section>

    <?php
          include_once 'includes/footer.php';
    ?>
</body>
</html>