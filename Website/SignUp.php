<?php
include_once 'includes/header.php';
require_once 'includes/ErrorHandling.php';
?>
 <!DOCTYPE html>
<html lang="en">

<head>
    <title>Sign Up</title>
</head>

<body>
   <section class="section1">
        <article class="overflowbox"> 
            <h1>Sign Up</h1>
            <!--Form that creates the user info-->
                <form action="includes/SignUpInclude.php" method="post">
                    <label for="name"><b>Name:</b></label>
                    <input type="text" name="name" placeholder="Full name..." />
                    <br />
                    <br />
                    <label for="email"><b>Email:</b></label>
                    <input type="text" name="email" placeholder="Email..." />
                     <br />
                    <br />
                    <label for="uid"><b>Username:</b></label>
                    <input type="text" name="uid" placeholder="Username..." />
                    <br />
                    <br />
                    <label for="password"><b>Password:</b></label>
                    <input type="password" name="pwd" placeholder="Password..." />
                    <br />
                    <br />
                    <label for="password"><b>Repeat Password:</b></label>
                    <input type="password" name="pwdrepeat" placeholder="Repeat Password..." />
                    <br />
                    <br />
                    <button type="submit" name="submit">Sign Up</button>
                </form>
                    <br />
                    <br />
                 <?php 
                 //error handeling
                    SignUpErrorHandling();
                 ?>
        </article>

    </section>

<?php
  include_once 'includes/footer.php';
?>
</body>
</html>