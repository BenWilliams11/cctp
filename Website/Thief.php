<?php
include_once 'includes/header.php';
require_once 'includes/DisplayCharacterStats.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Thief</title>
</head>

<body>
    <section class="section1">
        <article class="overflowbox"> 
            <h1>Thief</h1>
            <img src="images/profilepic1.jpg" alt="Placeholder image" width="100" height="100" />
            <br />
            <table>
                <tr>
                <th>Class</th>
                <th>Level</th>
                <th>EXP Gained</th>
                <th>HP</th>
                <th>Atk</th>
                <th>Def</th>
                <th>Spd</th>
                <th>Rng</th>
                <th>Res</th>
                </tr>
                <?php
                //Character ID that gets passed into a funtion that displays the data in a table
                $charID =  "5";
                CharacterReferance($conn, $charID);
                $_SESSION['refID'] = "5";
                ?>
    
             <!--Form that when a name is written all of the refereance data, userid, username and the character name gets added to the table-->
            </table>
            <br/>
            <form action="includes/AddCharacterInclude.php" method="post">
                 <input type="text" name="charname" placeholder="Character Name" />
                 <br />
                <button type="submit" name="submit">Add Character</button>
            </form>
        </article> 
    </section>
     <?php
     include_once 'includes/footer.php';
     ?>
</body>
</html>