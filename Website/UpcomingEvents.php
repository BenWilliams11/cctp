    <?php
    include_once 'includes/header.php';
    ?>
<html lang="en">

<head>
    <title>Upcoming Events</title>
</head>

<body>
   <section class="section1">
        <article class="overflowbox"> 
            <h1>Upcoming Events</h1>
                <table>
                    <!--If you click on more info a new tab is opened with the pdf-->
                    <tr>
                        <th>Event</th>
                        <th>Date</th>
                        <th>Location</th>
                        <th>More Info</th>
                    </tr>
                    <tr>
                        <td>New Campaign Release</td>
                        <td>02/06/21</td>
                        <td>Online</td>
                        <th><a href="external_documents/event_info_1.pdf" target="_blank">More Info</a></th>
                    </tr>
                    <tr>
                        <td>Announcement of New Character</td>
                        <td>06/08/21</td>
                        <td>Online</td>
                         <th><a href="external_documents/event_info_2.pdf" target="_blank">More Info</a></th>
                    </tr>
                    <tr>
                        <td>Announcement of New Character</td>
                        <td>20/10/21</td>
                        <td>Online</td>
                         <th><a href="external_documents/event_info_3.pdf" target="_blank">More Info</a></th>
                    </tr>
                </table>
        </article>
    </section>

   <?php
      include_once 'includes/footer.php';
   ?>
</body>
</html>