<?php
include_once 'includes/header.php';
require_once 'includes/DisplayCharacterStats.php';
?>
 <html lang="en">

<head>
    <title>View Characters</title>
</head>

<body>
   <section class="section1">
        <article class="overflowbox"> 
            <h1>View Characters</h1>
                <img src="images/profilepic1.jpg" alt="Character image" width="100" height="100" />
                <br />
                <b>Fighter</b>
                <table>
                    <tr>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                   <?php
                   $charID = 1;
                   //displays the character info in the table
                   CharacterReferance($conn, $charID);
                   ?>
                </table>
                <br />
                <img src="images/profilepic2.jpg" alt="Character image" width="150" height="100" />
                <br />
                <b>Hunter</b>
                <table>
                    <tr>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                   <?php
                   $charID = 2;
                   //displays the character info in the table
                   CharacterReferance($conn, $charID);
                   ?>
                </table>
                <br />
                 <img src="images/profilepic3.jpg" alt="Character image" width="100" height="100" />
                <br />
                <b>Recruit</b>
                <table>
                    <tr>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                   <?php
                   $charID = 3;
                   //displays the character info in the table
                   CharacterReferance($conn, $charID);
                   ?>
                </table>
                <br />
                    <img src="images/profilepic4.jpg" alt="Character image" width="100" height="100" />
                <br />
                <b>Apprentice</b>
                <table>
                    <tr>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                   <?php
                   $charID = 4;
                   //displays the character info in the table
                   CharacterReferance($conn, $charID);
                   ?>
                </table>
                <br />
                <img src="images/profilepic1.jpg" alt="Character image" width="100" height="100" />
                <br />
                <b>Thief</b>
                <table>
                    <tr>
                        <th>Class</th>
                        <th>Level</th>
                        <th>EXP Gained</th>
                        <th>HP</th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Spd</th>
                        <th>Rng</th>
                        <th>Res</th>
                    </tr>
                   <?php
                   $charID = 5;
                   //displays the character info in the table
                   CharacterReferance($conn, $charID);
                   ?>
                </table>
            <br />
            <br />
            <br />
            <br />
            <br />
        </article>
    </section>
<?php
      include_once 'includes/footer.php';
?>
</body>
</html>