<?php
include_once 'DBH.php';
session_start();

//Sets the userid to the number of the user that is logged in
if (isset($_SESSION["userid"]))
{
    $userID = $_SESSION["userid"];
}

if (isset($_SESSION["useruid"]))
{ 
   $userUID = $_SESSION["useruid"];
}

//The name that the user typed in when creating the character
if (isset($_POST["submit"]))
{
    $name = $_POST['charname'];
}

//refID to determine which character is being displayed
$_SESSION['refID'];
$sqlref = "SELECT * FROM playercharacters_referance WHERE ID = ?;";
//Create a prepared statements
$stmt = mysqli_stmt_init($conn);
//Prepare the prepared statement
if (!mysqli_stmt_prepare($stmt, $sqlref))
{
    echo "SQL Statement Failed";
}
else
{
    //Bind parameters to the placeholder
    //if multiple placeholders put in ss or if 3 then sss + add another data var
    mysqli_stmt_bind_param($stmt, "s", $_SESSION['refID']);
    //Run parameters inside database
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);

    $row = mysqli_fetch_assoc($result);

    //sets each var to the referance tables data, if I change a characters stats I just need to change the referance table then the rest works
    $charID = $row["ID"];
    $class = $row["Class"];
    $level = $row["Level"];
    $exp = $row["EXP_Gained"];
    $health = $row["Health"];
    $attack = $row["Attack"];
    $defence = $row["Defence"];
    $speed = $row["Speed"];
    $atk_range = $row["Attack_Range"];
    $resistance = $row["Resistance"];
}

//Inserts all info into the database
$sql = "INSERT INTO playercharacters_update (UserID, usersUid, CharacterID, Name, Level, EXP_Gained, Class, Health, Attack, Defence, Speed, Attack_Range, Resistance)
             VALUES ('$userID', '$userUID', '$charID', '$name', '$level', '$exp', '$class', '$health', '$attack', '$defence', '$speed', '$atk_range', '$resistance')";

mysqli_query($conn, $sql);

header("Location: ../HomePage.php");