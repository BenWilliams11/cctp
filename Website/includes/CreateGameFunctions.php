<?php

//checks if any of the fields are empty 
function emptyCreateGame($username1, $characterName1, $username2, $characterName2, $username3, $charactername3, $username4, $charactername4)
{
    $result;

    if (empty($username1 || $characterName1 || $username2 || $characterName2 || $username3 || $charactername3 || $username4 || $charactername4))
    {
        $result = true;
    }
    else
    {
        $result = false;
    }
    return $result;
}

//function that sets the usernames and character names for the game when you input them in create game
function createGame($conn, $username1, $characterName1, $username2, $characterName2, $username3, $charactername3, $username4, $charactername4)
{
   $usernamecheck1 = createGameUserCheck($conn, $username1);
   $characternamecheck1 = createGameCharacterCheck($conn, $characterName1);

   $usernamecheck2 = createGameUserCheck($conn, $username2);
   $characternamecheck2 = createGameCharacterCheck($conn, $characterName2);

   $usernamecheck3 = createGameUserCheck($conn, $username3);
   $characternamecheck3 = createGameCharacterCheck($conn, $charactername3);

   $usernamecheck4 = createGameUserCheck($conn, $username4);
   $characternamecheck4 = createGameCharacterCheck($conn, $charactername4);
    
    if ($usernamecheck1 === false || $characternamecheck1 === false)
    {
        header("Location: ../CreateGame.php?error=incorrectuserorcharacter1");
        exit();
    }

    if ($usernamecheck2 === false || $characternamecheck2 === false)
    {
        header("Location: ../CreateGame.php?error=incorrectuserorcharacter2");
        exit();
    }

    if ($usernamecheck3 === false || $characternamecheck3 === false)
    {
        header("Location: ../CreateGame.php?error=incorrectuserorcharacter3");
        exit();
    }

    if ($usernamecheck4 === false || $characternamecheck4 === false)
    {
        header("Location: ../CreateGame.php?error=incorrectuserorcharacter4");
        exit();
    }

   session_start();
   $_SESSION["Player1Name"] =  $usernamecheck1["usersUid"];
   $_SESSION["Player1CharName"] =  $characternamecheck1["Name"];

   $_SESSION["Player2Name"] =  $usernamecheck2["usersUid"];
   $_SESSION["Player2CharName"] =  $characternamecheck2["Name"];

   $_SESSION["Player3Name"] =  $usernamecheck3["usersUid"];
   $_SESSION["Player3CharName"] =  $characternamecheck3["Name"];

   $_SESSION["Player4Name"] =  $usernamecheck4["usersUid"];
   $_SESSION["Player4CharName"] =  $characternamecheck4["Name"];

   header("Location: ../Game.php");
}

//checks if the username inputted exists
function createGameUserCheck($conn, $username)
{
    $sql = "SELECT * FROM users WHERE usersUid = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header("Location: ../CreateGame.php?error=nouserofname");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $username);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData))
    {
        return $row;
    }
    else
    {
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);
}

//checks if the character exists
function createGameCharacterCheck($conn, $characterName)
{
    $sql = "SELECT * FROM playercharacters_update WHERE Name = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header("Location: ../CreateGame.php?error=nocharofname");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $characterName);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData))
    {
        return $row;
    }
    else
    {
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);
}