<?php
require_once 'DBH.php';
require_once 'CreateGameFunctions.php';

//gets the info that the user typed into the forms in create game
 if (isset($_POST["submit"]))
 {
    $username1 = $_POST['player1name'];
    $charactername1 = $_POST['player1character'];

    $username2 = $_POST['player2name'];
    $charactername2 = $_POST['player2character'];

    $username3 = $_POST['player3name'];
    $charactername3 = $_POST['player3character'];

    $username4 = $_POST['player4name'];
    $charactername4 = $_POST['player4character'];    

    if (emptyCreateGame($username1, $characterName1, $username2, $characterName2, $username3, $charactername3, $username4, $charactername4) !== false)
    {
        header("Location: ../CreateGame.php?error=emptyinput");
        exit();
    }

    createGame($conn, $username1, $charactername1, $username2, $charactername2, $username3, $charactername3, $username4, $charactername4);

 }
else
{
    header("Location: ../CreateGame.php?error=emptyinput");
    exit();
}
