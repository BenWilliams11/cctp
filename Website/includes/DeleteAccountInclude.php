<?php
include_once 'DBH.php';
require_once 'DisplayCharacterStats.php';
session_start();

if (isset($_SESSION["userid"]))
{
    $userID = $_SESSION["userid"];
}

//calls a function from display character stats in which it deletes the user from the table and deletes all characters assosiated with that user
DeleteAccount($conn, $userID);

session_unset();
session_destroy();

header("Location: ../HomePage.php");