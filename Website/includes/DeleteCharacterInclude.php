<?php
include_once 'DBH.php';
require_once 'DisplayCharacterStats.php';
session_start();

$charname = mysqli_real_escape_string($conn, $_POST['charname']);

if (isset($_SESSION["userid"]))
{
    $userID = $_SESSION["userid"];
}

//Function that deletes character and refresh's the page
DeleteCharacter($conn, $charname, $userID);

