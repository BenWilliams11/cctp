<?php

//Function that displays a characters stats in a table, calls the referance table
function CharacterReferance($conn, $charID)
{
    $sql = "SELECT * FROM playercharacters_referance WHERE ID = ?;";
    //Create a prepared statements
    $stmt = mysqli_stmt_init($conn);
    //Prepare the prepared statement
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        echo "SQL Statement Failed";
    }
    else
    {
        //Bind parameters to the placeholder
        //if multiple placeholders put in ss or if 3 then sss + add another data var
        mysqli_stmt_bind_param($stmt, "s", $charID);
        //Run parameters inside database
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_assoc($result);
        //Display each row of the database in the table fields
        echo "
               <tr>
                      <td>".$row["Class"]."</td>
                      <td>".$row["Level"]."</td>
                      <td>".$row["EXP_Gained"]."</td>
                      <td>".$row["Health"]."</td>
                      <td>".$row["Attack"]."</td>
                      <td>".$row["Defence"]."</td>
                      <td>".$row["Speed"]."</td>
                      <td>".$row["Attack_Range"]."</td>
                      <td>".$row["Resistance"]."</td>
              </tr>";
    }

}

//displays the characters infomration in the game section
function CharacterUpdate($conn, $playername, $playercharname)
{
    $sql = "SELECT * FROM playercharacters_update WHERE usersUid = ? AND Name = ?;";
    //Create a prepared statements
    $stmt = mysqli_stmt_init($conn);
    //Prepare the prepared statement
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        echo "SQL Statement Failed";
    }
    else
    {
        //Bind parameters to the placeholder
        //if multiple placeholders put in ss or if 3 then sss + add another data var
        mysqli_stmt_bind_param($stmt, "ss", $playername, $playercharname);
        //Run parameters inside database
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);

        $row = mysqli_fetch_assoc($result);
        echo "
              <tr>
                     <td>".$row["Name"]."</td>
                     <td>".$row["Class"]."</td>
                     <td>".$row["Level"]."</td>
                     <td>".$row["EXP_Gained"]."</td>
                     <td>".$row["Health"]."</td>
                     <td>".$row["Attack"]."</td>
                     <td>".$row["Defence"]."</td>
                     <td>".$row["Speed"]."</td>
                     <td>".$row["Attack_Range"]."</td>
                     <td>".$row["Resistance"]."</td>
              </tr>";
    }
}

//loops through all characters that the user has and displays that info
function ProfileCharacterStats($conn, $userID)
{
    $sql = "SELECT * FROM playercharacters_update WHERE UserID = ?;";
    //Create a prepared statements
    $stmt = mysqli_stmt_init($conn);
    //Prepare the prepared statement
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        echo "SQL Statement Failed";
    }
    else
    {
        //Bind parameters to the placeholder
        //if multiple placeholders put in ss or if 3 then sss + add another data var
        mysqli_stmt_bind_param($stmt, "s", $userID);
        //Run parameters inside database
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $resultCheck = mysqli_num_rows($result);
        if($resultCheck > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
                echo "
                     <tr>
                            <td>".$row["Name"]."</td>
                            <td>".$row["Class"]."</td>
                            <td>".$row["Level"]."</td>
                            <td>".$row["EXP_Gained"]."</td>
                            <td>".$row["Health"]."</td>
                            <td>".$row["Attack"]."</td>
                            <td>".$row["Defence"]."</td>
                            <td>".$row["Speed"]."</td>
                            <td>".$row["Attack_Range"]."</td>
                            <td>".$row["Resistance"]."</td>
                     </tr>";
            }
        }
    }
}

//function that gets the users id and deletes that user in the user table, it also loops through all characters and deletes them aswell
function DeleteAccount($conn, $userID)
{
    $sql = "DELETE FROM users WHERE usersId = ?;";
    $sql1 = "DELETE FROM playercharacters_update WHERE UserID = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
       echo "SQL Error";
    }
        
    else
    {
        mysqli_stmt_bind_param($stmt, "s", $userID);
        mysqli_stmt_execute($stmt);
    }
    
    $stmt1 = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt1, $sql1))
    {
       echo "SQL Error";
    }
        
    else
    {
        mysqli_stmt_bind_param($stmt1, "s", $userID);
        mysqli_stmt_execute($stmt1);
    }
}

//function that takes the input the user puts in and deletes the character with that name
function DeleteCharacter($conn, $charname, $userID)
{
    $sql = "DELETE FROM playercharacters_update WHERE Name = ? AND UserID = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        echo "SQL Error";
    }
    else
    {
        mysqli_stmt_bind_param($stmt, "ss", $charname, $userID);
        mysqli_stmt_execute($stmt);
    }

    header("Location: ../Profile.php");
}

//function that is used to display the info needed for the leaderboards
function DisplayUsersCharacters($conn)
{
    $sql = "SELECT * FROM playercharacters_update;";
    //Create a prepared statements
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            echo "
                 <tr>
                        <td>".$row["Name"]."</td>
                        <td>".$row["Class"]."</td>
                        <td>".$row["Attack"]."</td>
                        <td>".$row["Level"]."</td>
                        <td>".$row["EXP_Gained"]."</td>
                 </tr>";
        }
    }
}

//displays the enemy stats, hardcoded for now
function DisplayEnemyStatistics($conn, $enemyID)
{
    $sql = "SELECT * FROM enemycharacters_referance WHERE EnemyID = ?;";
    //Create a prepared statements
    $stmt = mysqli_stmt_init($conn);
    //Prepare the prepared statement
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        echo "SQL Statement Failed";
    }
    else
    {
        //Bind parameters to the placeholder
        //if multiple placeholders put in ss or if 3 then sss + add another data var
        mysqli_stmt_bind_param($stmt, "s", $enemyID);
        //Run parameters inside database
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        $resultCheck = mysqli_num_rows($result);
        if($resultCheck > 0)
        {
            while($row = mysqli_fetch_assoc($result))
            {
            echo "
                 <tr>
                        <td>".$row["Enemy"]."</td>
                        <td>".$row["Health"]."</td>
                        <td>".$row["Attack"]."</td>
                        <td>".$row["Defence"]."</td>
                        <td>".$row["Speed"]."</td>
                        <td>".$row["Attack_Range"]."</td>
                        <td>".$row["Resistance"]."</td>
                        <td>".$row["EXP when hit"]."</td>
                        <td>".$row["EXP when killed"]."</td>
                 </tr>";
            }
        }
    }
}


 