<?php
//handles the create game errors and displays them as text in the box
function CreateGameErrorHandling()
{
    if (isset($_GET["error"]))
    {
        if ($_GET["error"] == "emptyinput")
        {
            echo "<b>Fill in all fields!</b>";
        }

        if ($_GET["error"] == "incorrectuserorcharacter1")
        {
            echo "<b>Player 1's Username or Character name is wrong or doesn't exist</b>";
        }

        if ($_GET["error"] == "incorrectuserorcharacter2")
        {
            echo "<b>Player 2's Username or Character name is wrong or doesn't exist</b>";
        }

        if ($_GET["error"] == "incorrectuserorcharacter3")
        {
            echo "<b>Player 3's Username or Character name is wrong or doesn't exist</b>";
        }

        if ($_GET["error"] == "incorrectuserorcharacter4")
        {
            echo "<b>Player 4's Username or Character name is wrong or doesn't exist</b>";
        }
    }
}

//handles log in errors like empty fields
function LoginErrorHandling()
{
  if (isset($_GET["error"]))
  {
    if ($_GET["error"] == "emptyinput")
    {
        echo "<b>Fill in all fields!</b>";
    }
    
    else if ($_GET["error"] == "wronglogin")
    {
        echo "<b>Incorrect Login Info</b>";
        
    }
  }
}

//handles the sign up errors
function SignUpErrorHandling()
{
  if (isset($_GET["error"]))
  {
     if ($_GET["error"] == "emptyinput")
     {
         echo "<b>Fill in all fields!</b>";
     }
     
     else if ($_GET["error"] == "invaliduid")
     {
         echo "<b>Choose a proper username!</b>";
         
     }

     else if ($_GET["error"] == "emptyemail")
     {
         echo "<b>Choose a proper email!</b>";
     }

     else if ($_GET["error"] == "passwordsdontmatch")
     {
         echo "<b>Passwords dont match</b>";
     }

     else if ($_GET["error"] == "stmtfailed")
     {
         echo "<b>Something went wrong</b>";
     }

     else if ($_GET["error"] == "usernametaken")
     {
         echo "<b>Username is taken</b>";
     }

     else if ($_GET["error"] == "none")
     {
         echo "<b>You have signed up</b>";
     }
  }
}