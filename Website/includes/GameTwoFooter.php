<footer>
    <script src="includes/Navigation.js"></script>
    <?php
    //If a user is signed in then display different buttons that if no one was signed in
    if (isset($_SESSION["useruid"]))
    {
    //display username
        echo "<p>".$_SESSION["useruid"]."</p>";
        echo "<button onclick= 'UpcomingEventsButton()'> Upcoming Events</button>";
        echo "<button onclick='ViewCharsButton()' >View Characters</button>";
        echo "<button onclick= 'LogOutButton()'> Log out</button>";
        echo "<button onclick= 'ProfileButton()'> Profile Page</button>";
    }

    else
    {
    //else display as guest
        echo "<p>Guest</p>";
        echo "<button onclick= 'UpcomingEventsButton()'> Upcoming Events</button>";
        echo "<button onclick='ViewCharsButton()' >View Characters</button>";
        echo "<button onclick= 'SignUpButton()'> Sign Up</button>";
        echo "<button onclick= 'LoginButton()'> Login</button>";
    }
    ?>
</footer>
