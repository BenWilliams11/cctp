<?php
    include_once 'includes/DBH.php';
    session_start();
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel= "stylesheet" href= "styles/gametwo.css">
</head>
    <body>
        <header>

            <logo>
                <img src="images/game2_logo.jpg" alt="Dark Souls 3 Logo" width="150" height="150">
            </logo>

            <!--Calls the javascript functions that switch pages when the buttons are clicked-->
            <script src="includes/Navigation.js"></script>
            <nav>
                <?php
                if (isset($_SESSION["useruid"]))
                {
                    echo "<button onclick= 'HomePageButton()' >Home Page</button>";
                    echo "<button onclick= 'CreateGameButton()' >Create Game</button>";
                    echo "<button onclick= 'HowToPlayButton()' >How to Play</button>";
                    echo "<button onclick= 'LeaderboardsButton()'>Leaderboards</button>";
                }

                else
                {
                    echo "<button onclick='HomePageButton()'>Home Page</button>";
                    echo "<button onclick='HowToPlayButton()'>How to Play</button>";
                    echo "<button onclick='LeaderboardsButton()'>Leaderboards</button>";
                }

                ?>

            </nav>
        </header>

    </body>
</html>
    