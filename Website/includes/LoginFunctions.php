<?php

//checks if the inputs in sign up are empty
function emptyInputSignup($name, $email, $username, $pwd, $pwdrepeat)
{
    $result;

    if (empty($name || $email || $username || $pwd || $pwdrepeat))
    {
        $result = true;
    }
    else
    {
        $result = false;
    }
    return $result;
}

//checks if the username is valid
function invalidUid($username)
{
    $result;

    if (!preg_match("/^[a-zA-Z0-9]*$/", $username))
    {
        $result = true;
    }
    else
    {
        $result = false;
    }
    return $result;
}

//checks if the email is valid
function invalidEmail($email)
{
    $result;

    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $result = true;
    }
    else
    {
        $result = false;
    }
    return $result;
}

//takes the input from both password forms and compares them if they match we continue if not an error is thrown
function pwdMatch($pwd, $pwdrepeat)
{
    $result;

    if ($pwd != $pwdrepeat)
    {
        $result = true;
    }
    else
    {
        $result = false;
    }
    return $result;
}

//checks the users table if the username exists already
function uidExists($conn, $username, $email)
{
    $sql = "SELECT * FROM users WHERE usersUid = ? OR usersEmail = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header("Location: ../SignUp.php?error=stmtfailed");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ss", $username, $email);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData))
    {
        return $row;
    }
    else
    {
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);
}

//adds the data the user inputted and creats a new user that can log in
function createUser($conn, $name, $email, $username, $pwd)
{
    $sql = "INSERT INTO users (usersName, usersEmail, usersUid, usersPwd) VALUES (?, ?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header("Location: ../SignUp.php?error=stmtfailed");
        exit();
    }

    $hasedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "ssss", $name, $email, $username, $hasedPwd);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    header("Location: ../SignUp.php?error=none");
}

//checks if log in info is empty
function emptyInputLogin($username, $pwd)
{
    $result;

    if (empty($username || $pwd))
    {
        $result = true;
    }
    else
    {
        $result = false;
    }
    return $result;
}

//checks if the info is correct and logs the user in
function loginUser($conn, $username, $pwd)
{
    $uidExists = uidExists($conn, $username, $username);

    if ($uidExists === false)
    {
        header("Location: ../Login.php?error=wronglogin");
        exit();
    }

    $pwdHased = $uidExists["usersPwd"];

    $checkPwd = password_verify($pwd, $pwdHased);

    if ($checkPwd === false)
    {
        header("Location: ../Login.php?error=wronglogin");
        exit();
    }

    else if ($checkPwd === true)
    {
        session_start();
        $_SESSION["userid"] =  $uidExists["usersId"];
        $_SESSION["useruid"] =  $uidExists["usersUid"];

        header("Location: ../HomePage.php");
        exit();
    }
}





