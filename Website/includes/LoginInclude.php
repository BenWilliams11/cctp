<?php
require_once 'DBH.php';
require_once 'LoginFunctions.php';

if (isset($_POST["submit"]))
{
    $username = $_POST['uid'];
    $pwd = $_POST['pwd'];

    if (emptyInputLogin($username, $pwd) !== false)
    {
        header("Location: ../Login.php?error=emptyinput");
        exit();
    }

    //calls the function in LoginFunctions.php to log the user in
    loginUser($conn, $username, $pwd);
}

else
{
    header("Location: ../Login.php?error=emptyinput");
    exit();
}