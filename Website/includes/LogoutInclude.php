<?php
//destroys the session thus logging out the user
session_start();

session_unset();

session_destroy();

header("Location: ../HomePage.php");
exit();