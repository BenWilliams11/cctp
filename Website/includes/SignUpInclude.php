<?php
require_once 'DBH.php';
require_once 'LoginFunctions.php';

if (isset($_POST["submit"]))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $username =  $_POST['uid'];
    $pwd = $_POST['pwd'];
    $pwdrepeat = $_POST['pwdrepeat'];

    //Error Messages
    //if anything besides false send error
    if (emptyInputSignup($name, $email, $username, $pwd, $pwdrepeat) !== false)
    {
        header("Location: ../SignUp.php?error=emptyinput");
        exit();
    }

    //if anything besides false send error
    if (invalidUid($username) !== false)
    {
        header("Location: ../SignUp.php?error=invaliduid");
        exit();
    }

    //if anything besides false send error
    if (invalidEmail($email) !== false)
    {
        header("Location: ../SignUp.php?error=emptyemail");
        exit();
    }

    //if anything besides false send error
    if (pwdMatch($pwd, $pwdrepeat) !== false)
    {
        header("Location: ../SignUp.php?error=passwordsdontmatch");
        exit();
    }

    //if anything besides false send error
    if (uidExists($conn, $username, $email) !== false)
    {
        header("Location: ../SignUp.php?error=usernametaken");
        exit();
    }

    createUser($conn, $name, $email, $username, $pwd);           
}
else
{
    header("Location: ../SignUp.php");
    exit();
}

header("Location: ../Login.php");