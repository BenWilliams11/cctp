function sortTable(n)
{
    var table;
    var rows;
    var switching;
    var i;
    var x;
    var y;
    var shouldSwitch;
    var direction;
    var switchcount = 0;

    table = document.getElementById("table");
    switching = true;
    // Set the sorting direction to ascending:
    direction = "asc";
  
    while (switching)
    {
        switching = false;
        rows = table.rows;
        // Loop through all table rows except the first, which contains table headers
        for (i = 1; i < (rows.length - 1); i++)
        {
            shouldSwitch = false;
            
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
           
            if (direction == "asc")
            {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase())
                {
                    shouldSwitch = true;
                    break;
                }
            }

            else if (direction == "desc")
            {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase())
                {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch)
        {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        }
        else
        {
            if (switchcount == 0 && direction == "asc")
            {
                direction = "desc";
                switching = true;
            }
        }
    }
}