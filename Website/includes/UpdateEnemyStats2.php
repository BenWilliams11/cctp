<?php
include_once 'DBH.php';

//takes the health value that the game master inputs and updates it in the table, this value gets read back in game.php

$health = mysqli_real_escape_string($conn, $_POST['health']);

$sql = "UPDATE enemycharacters_referance SET 
Health = ?
WHERE EnemyID = 2;";

$stmt = mysqli_stmt_init($conn);
if (!mysqli_stmt_prepare($stmt, $sql))
{
    echo "SQL Error";
}
else
{
    mysqli_stmt_bind_param($stmt, "s", $health);
    mysqli_stmt_execute($stmt);
}

header("Location: ../Game.php");
?>