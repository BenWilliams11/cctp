<?php

//function that takes all of the values that the game master inputs and updates the correct characters stats by passing in the player and character name
function UpdateCharacterStats($conn, $playername, $playercharactername)
{
    //to prevent sql injection
    $level = mysqli_real_escape_string($conn, $_POST['level']);
    $exp = mysqli_real_escape_string($conn, $_POST['exp']);
    $health = mysqli_real_escape_string($conn, $_POST['health']);
    $attack =  mysqli_real_escape_string($conn, $_POST['attack']);
    $defence =  mysqli_real_escape_string($conn, $_POST['defence']);
    $speed =  mysqli_real_escape_string($conn, $_POST['speed']);
    $range =  mysqli_real_escape_string($conn, $_POST['attack_range']);
    $resistance =  mysqli_real_escape_string($conn, $_POST['resistance']);

    $sql = "UPDATE playercharacters_update SET
       Level = ?,
       EXP_Gained = ?,
       Health = ?,
       Attack = ?,
       Defence = ?,
       Speed = ?,
       Attack_Range = ?,
       Resistance = ?
       WHERE usersUid = ? AND Name = ?";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        echo "SQL Error";
    }
    else
    {
        mysqli_stmt_bind_param($stmt, "ssssssssss", $level, $exp, $health, $attack, $defence, $speed, $range, $resistance, $playername, $playercharactername);
        mysqli_stmt_execute($stmt);
    }

    header("Location: ../Game.php");
}