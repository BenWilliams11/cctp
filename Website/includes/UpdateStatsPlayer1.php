<?php
include_once 'DBH.php';
require_once 'UpdateStats.php';
session_start();

//Sets the userid to the number of the user that is logged in
if (isset($_SESSION["Player1Name"]))
{
    $player1name = $_SESSION["Player1Name"];
}

if (isset($_SESSION["Player1CharName"]))
{
   $player1charname = $_SESSION["Player1CharName"];
}

UpdateCharacterStats($conn, $player1name, $player1charname);