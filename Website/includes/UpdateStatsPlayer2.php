<?php
include_once 'DBH.php';
require_once 'UpdateStats.php';
session_start();

//Sets the userid to the number of the user that is logged in
if (isset($_SESSION["Player2Name"]))
{
    $player2name = $_SESSION["Player2Name"];
}

if (isset($_SESSION["Player2CharName"]))
{
   $player2charname = $_SESSION["Player2CharName"];
}

UpdateCharacterStats($conn, $player2name, $player2charname);