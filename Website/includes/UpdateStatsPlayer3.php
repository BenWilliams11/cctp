<?php
include_once 'DBH.php';
require_once 'UpdateStats.php';
session_start();

//Sets the userid to the number of the user that is logged in
if (isset($_SESSION["Player3Name"]))
{
    $player3name = $_SESSION["Player3Name"];
}

if (isset($_SESSION["Player3CharName"]))
{
   $player3charname = $_SESSION["Player3CharName"];
}

UpdateCharacterStats($conn, $player3name, $player3charname);