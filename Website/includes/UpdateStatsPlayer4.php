<?php
include_once 'DBH.php';
require_once 'UpdateStats.php';
session_start();

//Sets the userid to the number of the user that is logged in
if (isset($_SESSION["Player4Name"]))
{
    $player4name = $_SESSION["Player4Name"];
}

if (isset($_SESSION["Player4CharName"]))
{
   $player4charname = $_SESSION["Player4CharName"];
}

UpdateCharacterStats($conn, $player4name, $player4charname);