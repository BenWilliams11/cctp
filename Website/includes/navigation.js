//Every page on this website that we want to navigate to is here, click on the button and it takes you to the page corrisponding to the function called

function HomePageButton()
{
    window.location.href = "HomePage.php";
}

function CreateGameButton()
{
    window.location.href = "CreateGame.php";
}

function HowToPlayButton()
{
    window.location.href = "HowtoPlay.php";
}

function UpcomingEventsButton()
{
    window.location.href = "UpcomingEvents.php";
}

function ProfileButton()
{
    window.location.href = "Profile.php";
}

function LogOutButton()
{
    window.location.href = "includes/LogoutInclude.php";
}

function SignUpButton()
{
    window.location.href = "SignUp.php";
}

function LoginButton()
{
    window.location.href = "Login.php";
}

function LeaderboardsButton()
{
    window.location.href = "Boards.php";
}

function ViewCharsButton()
{
    window.location.href = "ViewChars.php";
}
